# Introduction

PIKA is a job performance monitoring stack specifically developed for the TU Dresden cluster to identify potentially inefficient jobs. Over the years, numerous extensions have been made with the goal of making it as portable as possible, although this remains a significant challenge in HPC.

It consists of:

- Performance Data Collector: An extension of the collection daemon collectd to record performance data on each compute node
- Job Metadata Collector: Slurm's `scontrol` tool in prolog and epilog or Slurm PrEp plugin as RabbitMQ producer
- Frontend: Powerful interactive GUI based on Angular
- Backend/Analysis Server: Rest-API, post-processing, job metadata collector as RabbitMQ consumer

PIKA uses InfluxDB to store metric data and MariaDB to store job metadata. Most of the above components are available as Docker containers and RPM packages.

PIKA is under permanent development. If you have any questions or encounter bugs, please don't hesitate to [contact](mailto:frank.winkler@tu-dresden.de) us.

<!---  To learn more about PIKA, read [the wiki](../wikis/home). --->

# Installation
The installation of PIKA on a cluster requires several steps. Firstly, resources should be provided for the PIKA services. It is advisable to provide a separate machine or virtual machine for each individual service. Once the services have been installed, the PIKA packages must be rolled out on both the Slurm controller nodes and the compute nodes. An overview of our installation at TU Dresden can be found [here](pika-components.png).

<!-- **Hint:** If you encounter problems with permissions when installing Docker containers, set `umask 0002` before `git clone` and keep it for the entire build session or set it again for subsequent changes.
```
umask 0002
git clone https://gitlab.hrz.tu-chemnitz.de/pika/pika-packages.git
```


1. [PIKA services](#1-pika-services)
2. [Performance data collector](#2-performance-data-collector)
3. [Job metadata collector](#3-job-metadata-collector)
4. [Final adjustments](#4-final-adjustments)
--->

## 1. PIKA services

There are four Docker containers: `pika-mariadb`, `pika-influxdb`, `pika-server` and `pika-web`.

Make sure that [Docker Engine](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) is installed on each host.

It's recommended to install these four containers on separate hosts or virtual machines in the following order:

1. [pika-mariadb](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-mariadb)
2. [pika-influxdb](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-influxdb)
3. [pika-server](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-server)
4. [pika-web](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-web)

For the individual components, we recommend the following hardware specifications:

| Container      | Host Specification |
| ------         | ------ |
|  pika-mariadb  | 4 cores, 32GB RAM, 200GB SSD, backup mount point for regular sql dumps       |
|  pika-influxdb | 24 cores, 64GB RAM, 1-2TB SSD (magnitude of 5K monitored nodes and retention policy of one year)|
|  pika-server   | 8 cores, 16GB RAM |
|  pika-web      | 2 cores, 2GB RAM |


**Note**:
All containers are installed with the external bridge network `pika-net`.
If you install containers on the same host, e.g. `pika-mariadb` and `pika-server`, you have to use the service name `pika-mariadb` as a host in `pika-server` in order to connect to the PIKA database. Note that `localhost` does not work when communicating between containers on the same host.


## 2. PIKA data collector

The PIKA data collector [pika-collectd](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-data-collector) is a daemon that must be installed on each compute node.
Follow the instructions [here](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-data-collector).

## 3. PIKA job collector
PIKA offers two options for collecting job metadata. One is based on the Slurm `scontrol` tool and is executed in `prolog` and `epilog`, while the other is based on the Slurm PrEp plugin.
Follow the instructions [here](https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-job-collector).

## 4. Final adjustments
If both `pika-collectd` and the job metadata collector provide continuous data, the following adjustments should be made.

- Update partition definition
- Enable post-processing
- Adjust I/O metrics

### 4.1 Update cluster/partition definition
The `partition.json` file in `pika-server` must include all clusters (if your site has multiple clusters), along with the corresponding Slurm partition name and architecture definition.

Connect to the machine on which `pika-server` is running and adjust `partition.json`.

```sh
vi partition.json

[
  {
    ...
    "name": "new_partition",
    "cpu_num": 104,
    "socket_num": 2,
    "likwid_socket_cpu": "[0,52]",
    "smt_mode": 2,
    "gpu_num": 0,
    "likwid_arch_name": "SPR",
    "node_list": "n[1001-1630]",
    "max_mem_bw": 65600000000,
    "max_flops": 40000000000,
    "max_ipc": 10,
    "active": 1,
    "cluster": "barnard",
    "mem_per_node": 549755813888
  }
]

```
After this adjustment, `pika-server` must be reinstalled. You can use the `update.sh` script.

```sh
./update.sh
```

### 4.2 Enable post-processing
During post-processing, jobs are analyzed daily at 2 a.m. First, so-called footprints are created, which are derived from aggregated values (mean, maximum) of the continuous performance data. This allows a group of jobs to be represented in histograms or scatter plots. Furthermore, jobs are characterized and assigned to categories such as compute-bound or memory-bound. Lastly, jobs are examined for performance issues. In this process, jobs with extremely high idle resources are captured and can later be investigated using the `Issue` component.

Connect to the machine on which `pika-server` is running and adjust `pika.conf`.
```sh
vi pika.conf

[post-processing]
execution_schedule = "0 2 * * *"
```
After this adjustment, you must reinstall `pika-server` using the `update.sh` script.

### 4.3 Modify I/O metrics

The I/O metrics must be configured in `pika-collectd`, `pika-server`, and `pika-web`. The following describes the inclusion of Lustre I/O metrics from a specific Lustre instance.

#### Selection of the Lustre instance
```sh
lfs getname
...
scratch-ff29144287fbd800 /data/horse
```
We demonstrate how to record I/O metrics from `/data/horse` in PIKA.

#### Add Lustre I/O instance in pika-collectd
```sh
vi pika-collectd.conf
...
Import "lustre_bw"
  <Module lustre_bw>
    fsname_and_mount "*:/data/horse"
    recheck_limit 360
  </Module>
```
After this adjustment, `pika-collectd` must be restarted on each compute node.


#### Add Lustre I/O instance in pika-server
```sh
vi metric.json
...
"type": "lustre",
        "instances": [
          {
            "name": "horse",
            "measurement": "lustre_scratch"
          },
...
```

Note that the suffix in `measurement` contains the prefix from the lustre instance (`lfs getname`: `scratch-...`, in this example lustre`_scratch`.

After this adjustment, you must reinstall `pika-server` using the `update.sh` script.

#### Add Lustre I/O instance in pika-web
```sh
vi config.json
{
  ...
  "FileSystemInstanceNames": [..., "lustre_horse"],
  "FileSystems": {
    "io": {
      "lustre_io": {
        "label": "Lustre",
        "options": {
          ...
          "horse_read_bw": "Horse Read Bandwidth ",
          "horse_write_bw": "Horse Write Bandwidth "
        }
      }
    },
    "io_meta": {
      "lustre_io_meta": {
        "label": "Lustre",
        "options": {
          ...
          "horse_read_requests": "Horse Read Requests ",
          "horse_write_requests": "Horse Write Requests ",
          "horse_open": "Horse Open ",
          "horse_close": "Horse Close ",
          "horse_fsync": "Horse Fsync ",
          "horse_create": "Horse Create ",
          "horse_seek": "Horse Seek "
        }
      }
    }
  }
  ...
}
```
With these settings, all I/O metrics under the category `horse` will be listed in the I/O display menu with the corresponding caption.

Note that `FileSystemInstanceNames` uses `horse` as a suffix, in this example lustre`_horse`.

After this adjustment, you must reinstall `pika-web` using the `update.sh` script.

---
Finally, several jobs with a minimum runtime of approximately 4 minutes should be submitted to validate both the job metadata and performance data in `pika-web`.
